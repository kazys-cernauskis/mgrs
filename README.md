# MGRS žemėlapio sugeneravimas

## Parsisiuntimai
 - QGIS programa [3.28](https://qgis.org/downloads/QGIS-OSGeo4W-3.28.10-1.msi)
 - Military grids add-on [Download](https://plugins.qgis.org/geopackages/7/download/)

 ## Sukonfigūravimas
 ### Pradinio projekto susikūrimas
  - Susidiegiame QGIS programą
  - Susikuriame katalogą kuriame išsisaugosime žemėlapio projektą (pvz. ../Documents/mgrs/)
  - QGIS programoje susikuriame naują projektą `project`->`new`
  - QGIS programoje išsaugome projektą prieš tai susikurtame kataloge `project`->`save as` (pvz. ../Documents/mgrs/zemelapis.qgz)

### Lat Lon plugin įdiegimas
  - QGIS programoje atsidarome `plugin` langą: `Plugins`-> `Manage and install plugins...`
  - Paieškos laukelyje suvedame `Lat Lon`. Spaudžiame `enter`. Paieška turi rodyti vieną rastą plugin'a - `Lat Lon Tools` 
  - Pažymime šitą plugin'ą ir spaudžiame `Install`

### Military grid add-on paruošimas
  - Išjungiame QGIS programą
  - Išarchyvuojame `Military grids add-on` parsisiūsta paketą `military-grids-tools-and-example-data-and-layouts.zip` į laikiną katalogą.
  - Išarchyvuotame kataloge turime rasti failą `QGIS_Military_grids_LzS3XF7.gpkg` (Pastaba: paskutiniai 7 simboliai gali skirtis `_bla1234.qpkg`)
  - Šitą failą nusikopijuojame į projekto katalogą (pvz. ../Documents/mgrs)
  - Nukopijuotą failą pervadiname nutrindami paskutinius 8 simbolius. Turi gautis toks pavadinimas `QGIS_Military_grids.gpkg`
  - Startuojame QGIS programą ir atsidarome prieš tai išsaugotą žemėlapio projektą
  - 
### Topo50LT žemėlapio prijungimas

## MGRS tinklelio generavimas

## Žemėlapio išdėstymas spauzdinimui